#include "net/tcp_server.hpp"
#include "base/logger.h"
#include <iostream>

using namespace easyasio::net;

int main(int argc, char* argv[])
{
    try
    {
        std::shared_ptr<TcpServer> s(std::make_shared<TcpServer>("0.0.0.0", "1234", 1));
    	s->setMessageCallback([](const TcpConnectionPtr& conn, Buffer* buf)
    	{
    		std::cout << std::string(buf->peek(), buf->readableBytes()) << std::endl;
    		conn->send(std::string(buf->peek(), buf->readableBytes()));
    		buf->retrieveAll();
    	} );

    	s->setConnectionCallback([](const TcpConnectionPtr& conn)
    	{
    		std::cout << "new connection" << std::endl;
    	});

    	s->run();

    	int a;
    	std::cin >> a;
    }
    catch (asio::system_error& e)
    { 
        LOG_ERROR("{}", e.what());
    }
    catch (...)
    {
        LOG_ERROR("unknow error");
    }
	


    return 0;
}