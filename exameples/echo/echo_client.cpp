#include "net/tcp_client.hpp"
#include "base/logger.h"
#include <iostream>

using namespace easyasio::net;

int main(int argc, char* argv[])
{
    try
    {
        asio::io_service loop;
        std::shared_ptr<TcpClient> c(std::make_shared<TcpClient>(loop, "127.0.0.1", 1234));
        c->setConnectionCallback([](const TcpConnectionPtr& conn)
        {
            conn->send(std::string("hello world!"));
            conn->forceClose();
        });

        c->setMessageCallback([](const TcpConnectionPtr& conn, Buffer* buff)
        {
            //conn->send(std::string(buff->peek(), buff->readableBytes()));
            //buff->retrieveAll();
        });
        c->asynConnect();
        loop.run();
    	int x;
    	std::cin >> x;
    }
    catch (asio::system_error& e)
    { 
        LOG_ERROR("{}", e.what());
    }
    catch (...)
    {
        LOG_ERROR("unknow error");
    }

    return 0;
}