#简介

<font size="20">easyasio是基于boost.asio网络库的封装， 同时依赖boost，protobuf库。 easyasio目标并不是为了高性能， 而是为了跨平台和易用性。方便对linux不熟悉的程序猿在windowns平台下编写在linux下跑的网络应用。</font>

##性能

性能方面参考muduo的pingpong的吞吐量的测试， 测试代码在exameples/pingpong目录下， 在这里只是与linux下的muduo pingpong做比较，muduo测试代码在muduo项目中有， 测试环境为虚拟机centos7, 4核cpu 3.60GHz, 内存2g. 测试的结论是对于一般大小的数据块easyasio较muduo有20-30%的性能损失， 对大数据块10k以上easyasio的性能只有muduo的20-30%。推测原因是:asio在linux下的封装有些过度, asio没提供用户缓冲, easyasio的send没做优化(在loopthread中调用不用post到ioservice)这些留待以后优化。不过easyasio对于编写性能要求一般的网络程序还是可以满足要求的。

- 小块数据吞吐量

  ![avatar](doc/1thread-1k.png)

  ![avatar](doc/1000conn-1k.png)

  ![avatar](doc/10000conn-1k.png)

- 大块数据吞吐量

  ![avatar](doc/1thread-16k.png)

  ![avatar](doc/1000conn-16k.png)

  ![avatar](doc/1000conn-16k.png)
