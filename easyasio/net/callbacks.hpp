#ifndef EASYASIO_NET_CALLBACKS_HPP 
#define EASYASIO_NET_CALLBACKS_HPP 

#include <functional>
#include <memory>

#include "buffer.hpp"

namespace easyasio {
namespace net {

class Buffer;
class TcpConnection;
typedef std::shared_ptr<TcpConnection> TcpConnectionPtr;
typedef std::function<void()> TimerCallback;
typedef std::function<void(const TcpConnectionPtr&)> ConnectionCallback;
typedef std::function<void(const TcpConnectionPtr&)> CloseCallback;
typedef std::function<void(const TcpConnectionPtr&)> WriteCompleteCallback;
typedef std::function<void(const TcpConnectionPtr&, size_t)> HighWaterMarkCallback;

typedef std::function<void(const TcpConnectionPtr&, Buffer*)> MessageCallback;

void defaultConnectionCallback(const TcpConnectionPtr& conn);
void defaultMessageCallback(const TcpConnectionPtr& conn, Buffer* buffer);

}//net
}//easyasio 

#endif  // EASYASIO_NET_CALLBACKS_HPP 
