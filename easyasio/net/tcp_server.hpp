#ifndef EASYASIO_NET_TCP_SERVER_HPP
#define EASYASIO_NET_TCP_SERVER_HPP

#include <asio.hpp>
#include <string>
#include <vector>
#include <memory>

#include "../base/utils.hpp"
#include "callbacks.hpp"
#include "io_service_pool.hpp"
#include "tcp_connection.hpp"

namespace easyasio {
namespace net {

class TcpServer : private noncopyable
{
public:
	explicit TcpServer(const std::string& address, const std::string& port, std::size_t io_service_pool_size);

	void run();

	void setConnectionCallback(const ConnectionCallback& cb) { connectionCallback_ = cb; }
	void setMessageCallback(const MessageCallback& cb) { messageCallback_ = cb; }
	void setWriteCompleteCallback(const WriteCompleteCallback& cb) { writeCompleteCallback_ = cb; }
    void setCloseCallback(const CloseCallback& cb) { closeCallback_ = cb; }

private:
	void start_accept();

	void handle_accept(const asio::error_code& e);

	void handle_stop();

	io_service_pool                 io_service_pool_;

	asio::signal_set                signals_;

	asio::ip::tcp::acceptor         acceptor_;

	TcpConnectionPtr                new_connection_;

	ConnectionCallback              connectionCallback_;
	MessageCallback                 messageCallback_;
	WriteCompleteCallback           writeCompleteCallback_;
    CloseCallback                   closeCallback_;
};

} // namespace net  
} // namespace easyasio  

#endif // EASYASIO_NET_TCP_SERVER_HPP
