#ifndef EASYASIO_PROTORPC_RPC_SERVER_HPP
#define EASYASIO_PROTORPC_RPC_SERVER_HPP

#include "../net/tcp_server.hpp"

namespace google {
namespace protobuf {

class Service;

}  // namespace protobuf
}  // namespace google

namespace easyasio
{
namespace net
{

class RpcServer
{
public:
    RpcServer(const std::string& address, const std::string& port, std::size_t io_service_pool_size);

    void registerService(::google::protobuf::Service*);
    void start();

private:
    void onConnection(const TcpConnectionPtr& conn);

    // void onMessage(const TcpConnectionPtr& conn,
    //                Buffer* buf,
    //                Timestamp time);

    TcpServer server_;
    std::map<std::string, ::google::protobuf::Service*> services_;
};

}
}

#endif  // EASYASIO_PROTORPC_RPC_SERVER_HPP
