// Copyright 2014 Renato Tegon Forti, Antony Polukhin.
// Copyright 2015 Antony Polukhin.
//
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt
// or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_DLL_LIBRARY_INFO_HPP
#define BOOST_DLL_LIBRARY_INFO_HPP

#include <boost/config.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/vector_c.hpp>
#include <boost/aligned_storage.hpp>
#include <boost/noncopyable.hpp>
#include <boost/predef/os.h>
#include <boost/predef/architecture.h>
#include <boost/type_traits/integral_constant.hpp>

#include <boost/dll/detail/pe_info.hpp>
#include <boost/dll/detail/elf_info.hpp>
#include <boost/dll/detail/macho_info.hpp>

#ifdef BOOST_HAS_PRAGMA_ONCE
# pragma once
#endif

/// \file boost/dll/library_info.hpp
/// \brief Contains only the mnb::dll::library_info class that is capable of
/// extracting different information from binaries.

namespace mnb { namespace dll {

/*!
* \brief Class that is capable of extracting different information from a library or binary file.
* Currently understands ELF, MACH-O and PE formats on all the platforms.
*/
class library_info: private mnb::noncopyable {
private:
    mnb::filesystem::ifstream f_;

    mnb::aligned_storage< // making my own std::aligned_union from scratch. TODO: move to TypeTraits
        mnb::mpl::deref<
            mnb::mpl::max_element<
                mnb::mpl::vector_c<std::size_t,
                    sizeof(mnb::dll::detail::elf_info32),
                    sizeof(mnb::dll::detail::elf_info64),
                    sizeof(mnb::dll::detail::pe_info32),
                    sizeof(mnb::dll::detail::pe_info64),
                    sizeof(mnb::dll::detail::macho_info32),
                    sizeof(mnb::dll::detail::macho_info64)
                >
            >::type
        >::type::value
    >::type impl_;

    /// @cond
    mnb::dll::detail::x_info_interface& impl() BOOST_NOEXCEPT {
        return *reinterpret_cast<mnb::dll::detail::x_info_interface*>(impl_.address());
    }

    inline static void throw_if_in_32bit_impl(mnb::true_type /* is_32bit_platform */) {
        mnb::throw_exception(std::runtime_error("Not native format: 64bit binary"));
    }

    inline static void throw_if_in_32bit_impl(mnb::false_type /* is_32bit_platform */) BOOST_NOEXCEPT {}


    inline static void throw_if_in_32bit() {
        throw_if_in_32bit_impl( mnb::integral_constant<bool, (sizeof(void*) == 4)>() );
    }

    static void throw_if_in_windows() {
#if BOOST_OS_WINDOWS
        mnb::throw_exception(std::runtime_error("Not native format: not a PE binary"));
#endif
    }

    static void throw_if_in_linux() {
#if !BOOST_OS_WINDOWS && !BOOST_OS_MACOS && !BOOST_OS_IOS
        mnb::throw_exception(std::runtime_error("Not native format: not an ELF binary"));
#endif
    }

    static void throw_if_in_macos() {
#if BOOST_OS_MACOS || BOOST_OS_IOS
        mnb::throw_exception(std::runtime_error("Not native format: not an Mach-O binary"));
#endif
    }

    void init(bool throw_if_not_native) {

        if (mnb::dll::detail::elf_info32::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_windows(); throw_if_in_macos(); }

            new (impl_.address()) mnb::dll::detail::elf_info32(f_);
        } else if (mnb::dll::detail::elf_info64::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_windows(); throw_if_in_macos(); throw_if_in_32bit(); }

            new (impl_.address()) mnb::dll::detail::elf_info64(f_);
        } else if (mnb::dll::detail::pe_info32::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_linux(); throw_if_in_macos(); }

            new (impl_.address()) mnb::dll::detail::pe_info32(f_);
        } else if (mnb::dll::detail::pe_info64::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_linux(); throw_if_in_macos(); throw_if_in_32bit(); }

            new (impl_.address()) mnb::dll::detail::pe_info64(f_);
        } else if (mnb::dll::detail::macho_info32::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_linux(); throw_if_in_windows(); }

            new (impl_.address()) mnb::dll::detail::macho_info32(f_);
        } else if (mnb::dll::detail::macho_info64::parsing_supported(f_)) {
            if (throw_if_not_native) { throw_if_in_linux(); throw_if_in_windows(); throw_if_in_32bit(); }

            new (impl_.address()) mnb::dll::detail::macho_info64(f_);
        } else {
            mnb::throw_exception(std::runtime_error("Unsupported binary format"));
        }
    }
    /// @endcond

public:
    /*!
    * Opens file with specified path and prepares for information extraction.
    * \param library_path Path to the binary file from which the info must be extracted.
    * \param throw_if_not_native_format Throw an exception if this file format is not
    * supported by OS.
    */
    explicit library_info(const mnb::filesystem::path& library_path, bool throw_if_not_native_format = true)
        : f_(library_path, std::ios_base::in | std::ios_base::binary)
        , impl_()
    {
        f_.exceptions(
            mnb::filesystem::ifstream::failbit
            | mnb::filesystem::ifstream::badbit
            | mnb::filesystem::ifstream::eofbit
        );

        init(throw_if_not_native_format);
    }

    /*!
    * \return List of sections that exist in binary file.
    */
    std::vector<std::string> sections() {
        return impl().sections();
    }

    /*!
    * \return List of all the exportable symbols from all the sections that exist in binary file.
    */
    std::vector<std::string> symbols() {
        return impl().symbols();
    }

    /*!
    * \param section_name Name of the section from which symbol names must be returned.
    * \return List of symbols from the specified section.
    */
    std::vector<std::string> symbols(const char* section_name) {
        return impl().symbols(section_name);
    }


    //! \overload std::vector<std::string> symbols(const char* section_name)
    std::vector<std::string> symbols(const std::string& section_name) {
        return impl().symbols(section_name.c_str());
    }

    /*!
    * \throw Nothing.
    */
    ~library_info() BOOST_NOEXCEPT {
        typedef mnb::dll::detail::x_info_interface T;
        impl().~T();
    }
};

}} // namespace mnb::dll
#endif // BOOST_DLL_LIBRARY_INFO_HPP
