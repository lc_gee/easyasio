// Copyright 2014 Renato Tegon Forti, Antony Polukhin.
// Copyright 2015 Antony Polukhin.
//
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt
// or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_DLL_DETAIL_WINDOWS_PATH_FROM_HANDLE_HPP
#define BOOST_DLL_DETAIL_WINDOWS_PATH_FROM_HANDLE_HPP

#include <boost/config.hpp>
#include <boost/dll/detail/system_error.hpp>
#include <boost/detail/winapi/dll.hpp>
#include <boost/detail/winapi/get_last_error.hpp>
#include <boost/filesystem/path.hpp>

#ifdef BOOST_HAS_PRAGMA_ONCE
# pragma once
#endif

namespace mnb { namespace dll { namespace detail {

    static inline mnb::system::error_code last_error_code() BOOST_NOEXCEPT {
        mnb::detail::winapi::DWORD_ err = mnb::detail::winapi::GetLastError();
        return mnb::system::error_code(
            err,
            mnb::system::system_category()
        );
    }

    inline mnb::filesystem::path path_from_handle(mnb::detail::winapi::HMODULE_ handle, mnb::system::error_code &ec) {
        BOOST_STATIC_CONSTANT(mnb::detail::winapi::DWORD_, ERROR_INSUFFICIENT_BUFFER_ = 0x7A);
        BOOST_STATIC_CONSTANT(mnb::detail::winapi::DWORD_, DEFAULT_PATH_SIZE_ = 260);

        // If `handle` parameter is NULL, GetModuleFileName retrieves the path of the
        // executable file of the current process.
        mnb::detail::winapi::WCHAR_ path_hldr[DEFAULT_PATH_SIZE_];
        mnb::detail::winapi::GetModuleFileNameW(handle, path_hldr, DEFAULT_PATH_SIZE_);
        ec = last_error_code();
        if (!ec) {
            return mnb::filesystem::path(path_hldr);
        }

        for (unsigned i = 2; i < 1025 && static_cast<mnb::detail::winapi::DWORD_>(ec.value()) == ERROR_INSUFFICIENT_BUFFER_; i *= 2) {
            std::wstring p(DEFAULT_PATH_SIZE_ * i, L'\0');
            const std::size_t size = mnb::detail::winapi::GetModuleFileNameW(handle, &p[0], DEFAULT_PATH_SIZE_ * i);
            ec = last_error_code();

            if (!ec) {
                p.resize(size);
                return mnb::filesystem::path(p);
            }
        }

        // Error other than ERROR_INSUFFICIENT_BUFFER_ occurred or failed to allocate buffer big enough
        return mnb::filesystem::path();
    }

}}} // namespace mnb::dll::detail

#endif // BOOST_DLL_DETAIL_WINDOWS_PATH_FROM_HANDLE_HPP

