#!/bin/bash
tar -xvf boost_1_64_0.tar.gz
cd boost_1_64_0
./bootstrap.sh
mkdir miniboost
./b2 tools/bcp
./dist/bin/bcp --namespace=mnb --namespace-alias interprocess miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias dll miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias algorithm miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias assert miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias program_options miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias date_time miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias crc miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias uuid miniboost

rm -fr miniboost/doc
rm -fr miniboost/boost.png
rm -fr miniboost/Jamroot

SRCDIR=`ls miniboost/libs/`
for srcdir in $SRCDIR
do
	echo $srcdir
	if [ ! -d "miniboost/libs/$srcdir/src" ]; then
		echo "miniboost/libs/$srcdir"
		rm -fr miniboost/libs/$srcdir
	else
		DIRS=`ls miniboost/libs/$srcdir`	
		for dir in $DIRS
		do
			if [ "$dir" != "src" ]; then
				rm -fr miniboost/libs/$srcdir/$dir
			fi
				
		done
	fi
done
